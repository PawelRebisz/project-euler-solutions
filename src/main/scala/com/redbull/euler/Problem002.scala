package com.redbull.euler

object Problem002 {
	def main (args: Array[String]) {
		println (s"answer: ${new FibonacciEvenValuedTerms().run(4000000)}")
	}
}

class FibonacciEvenValuedTerms {
	def run(upperConstrainOnTerm: Int): Long = {
		fibonacci
			.takeWhile { value => value < upperConstrainOnTerm }
			.filter { value => value % 2 == 0 }
			.sum
	}

	private def fibonacci:Stream[Int] = {
		Stream.cons(1,
			Stream.cons(1,
				(fibonacci zip fibonacci.tail) map { case (x, y) => x + y }
			)
		)
	}
}

