package com.redbull.euler

object Problem004 {
  def main(args: Array[String]) {
    println(s"answer: ${new LargestPalindrome().run(3)}")
  }
}

class LargestPalindrome {

  def run(numberOfDigits: Int): Int = {
    val upperBound: Int = ("9" * numberOfDigits).toInt
    val lowerBound: Int = ("1" + "0" * (numberOfDigits - 1)).toInt

    val palindromes: Seq[Int] = for {
      i <- upperBound until lowerBound by -1
      j <- upperBound until lowerBound by -1
      value = i * j
      if isPalindrome(value)
    } yield value

    palindromes.max
  }

  def isPalindrome(value: Int): Boolean = {
    val reversedValue: Int = value.toString.reverse.toInt
    value == reversedValue
  }
}

