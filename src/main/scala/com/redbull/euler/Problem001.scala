package com.redbull.euler

object Problem001 {
	def main (args: Array[String]) {
		println (s"answer: ${new MultiplesOfThreeAndFive().run(1000)}")
	}
}

class MultiplesOfThreeAndFive {
	def run(upperConstrain: Int): Long = {
		(1 to upperConstrain - 1)
			.filter({ value => value % 3 == 0 || value % 5 == 0})
			.sum
	}
}

