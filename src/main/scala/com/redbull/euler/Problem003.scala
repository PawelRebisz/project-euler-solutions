package com.redbull.euler

object Problem003 {
  def main(args: Array[String]) {
    println(s"answer: ${new LargestPrimeFactor().run(600851475143L)}")
  }
}

class LargestPrimeFactor {

  def run(givenValue: Long): Long = {
    val primeFactors: List[Long] = largestPrimeSoFar(givenValue, 2)
    primeFactors.max
  }

  def largestPrimeSoFar(givenValue: Long, startingPoint: Long): List[Long] = {
    var currentIteration = startingPoint
    while (givenValue % currentIteration != 0 && givenValue > startingPoint) {
      currentIteration = currentIteration + 1L
    }
    currentIteration match {
      case x if x < givenValue => x :: largestPrimeSoFar(givenValue / currentIteration, currentIteration)
      case x if x >= givenValue => x :: Nil
    }
  }
}

